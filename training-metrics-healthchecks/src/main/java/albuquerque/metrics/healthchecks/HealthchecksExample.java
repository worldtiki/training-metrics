package albuquerque.metrics.healthchecks;

import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;
import static com.codahale.metrics.MetricRegistry.name;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;

/**
 * Metrics also has the ability to centralize your service’s health checks with the metrics-healthchecks module.
 *
 * In this example our {@link ExampleHealthCheck} will randomly return 3 possible outcomes: healthy, unhealthy or will throw an exception.
 * Every time we print the status of the healthcheck it should display the last known outcome.
 */
public class HealthchecksExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(HealthchecksExample.class);
    private static final int CORE_POOL_SIZE = 1;
    private static final long INITIAL_DELAY = 0;
    private static final long INTERVAL_PERIOD = 1;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;

    private static final HealthCheckRegistry healthChecksRegistry = new HealthCheckRegistry();

    public static void main(String[] args) {
        // We start by naming our metric and registering it with the registry
        String metricName = name(HealthchecksExample.class, "execution-time");
        healthChecksRegistry.register(metricName, new ExampleHealthCheck());

        // We also schedule a task to periodically check the status of all our healthchecks
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        executor.scheduleAtFixedRate(() -> {
                    LOGGER.info("Checking healthchecks");
                    printHealthCheckStatus();
                },
                INITIAL_DELAY, INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);
    }

    private static void printHealthCheckStatus() {
        final Map<String, HealthCheck.Result> results = healthChecksRegistry.runHealthChecks();
        for (Map.Entry<String, HealthCheck.Result> entry : results.entrySet()) {
            if (entry.getValue().isHealthy()) {
                LOGGER.info("{} is healthy", entry.getKey());
            } else {
                LOGGER.warn("{} is unhealthy. Reason: {}", entry.getKey(), entry.getValue().getMessage());

                final Throwable e = entry.getValue().getError();
                if (e != null) {
                    e.printStackTrace();
                }
            }
        }
    }

}
