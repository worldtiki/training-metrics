package albuquerque.metrics.healthchecks;

import java.util.Random;

import com.codahale.metrics.health.HealthCheck;

public class ExampleHealthCheck extends HealthCheck {

    private static final int OK = 0;
    private static final int NOT_OK = 1;
    private static final int NOT_OK_EXCEPTION = 2;
    private static final int POSSIBLE_RESULTS = 3;
    private Random random = new Random();

    @Override
    public HealthCheck.Result check() throws Exception {
        int status = random.nextInt(POSSIBLE_RESULTS);

        switch (status) {
        case OK:
            return HealthCheck.Result.healthy();
        case NOT_OK:
            return HealthCheck.Result.unhealthy("Failure!");
        case NOT_OK_EXCEPTION:
            throw new RuntimeException("Ooops");
        }

        return null;
    }
}