package albuquerque.metrics.annotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Annotations can be used with the {@code metrics-spring} library.
 * Check the {@link Scenario} and {@link MeasurableBean} classes as well as the {@code application-config.xml} for more information
 * on how to set them up.
 */
public class AnnotationsExample {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-config.xml");

        Scenario scenario = context.getBean("scenario", Scenario.class);
        scenario.runScenario();
    }
}
