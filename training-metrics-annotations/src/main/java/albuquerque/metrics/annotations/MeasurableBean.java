package albuquerque.metrics.annotations;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;

/**
 * We are using multiple annotations in this bean.
 * The {@link Timed} tracks both calls/rates and execution times.
 * The {@link ExceptionMetered} on the other hand, only captures failures, ie, when an exception is thrown.
 * We can also use absolute names of the default fully qualified names for each metric.
 */
public class MeasurableBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeasurableBean.class);
    private static final int MAX_SLEEP_TIME = 100;
    private Random random = new Random();

    @Timed
    @ExceptionMetered
    public void doSomeOperation() {
        LOGGER.info("Doing some operation");

        int sleepTime = random.nextInt(MAX_SLEEP_TIME);
        sleepFor(sleepTime, MILLISECONDS);
    }

    @Timed(name = "anotherOp", absolute = true)
    @ExceptionMetered(name = "anotherOp.exceptions", absolute = true)
    public void doAnotherOperation() {
        LOGGER.info("Doing another operation");

        int sleepTime = random.nextInt(MAX_SLEEP_TIME);
        sleepFor(sleepTime, MILLISECONDS);
    }
}
