package albuquerque.metrics.annotations;

import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * On this scenario we will periodically execute our tasks.
 */
public class Scenario {

    private static final Logger LOGGER = LoggerFactory.getLogger(Scenario.class);

    private static final int CORE_POOL_SIZE = 1;
    private static final long INITIAL_DELAY = 0;
    private static final long INTERVAL_PERIOD = 1;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;

    private MeasurableBean measurableBean;

    public Scenario(MeasurableBean measurableBean) {
        this.measurableBean = measurableBean;
    }

    public void runScenario() {
        // We periodically execute our tasks
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        executor.scheduleAtFixedRate(() -> {
                    LOGGER.info("Running operations");
                    measurableBean.doSomeOperation();
                    measurableBean.doAnotherOperation();
                },
                INITIAL_DELAY, INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);

        // before stopping the executor service
        executor.shutdown();
    }
}
