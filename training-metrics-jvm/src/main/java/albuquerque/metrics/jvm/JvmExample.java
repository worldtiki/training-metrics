package albuquerque.metrics.jvm;

import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;
import static com.codahale.metrics.MetricRegistry.name;

import java.util.concurrent.TimeUnit;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;

/**
 * Example on how to use out of the nox jvm metrics.
 */
public class JvmExample {

    private static final long INTERVAL_PERIOD = 2;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;

    private static MetricRegistry registry = new MetricRegistry();

    public static void main(String[] args) {
        // We start by naming our metric and registering it with the registry
        registry.register(name(JvmExample.class, "jvm.gc"), new GarbageCollectorMetricSet());
        registry.register(name(JvmExample.class, "jvm.memory"), new MemoryUsageGaugeSet());
        registry.register(name(JvmExample.class, "jvm.thread-states"), new ThreadStatesGaugeSet());
        registry.register(name(JvmExample.class, "jvm.fd-usage"), new FileDescriptorRatioGauge());

        // Then we start our reporter and set it to run every couple of seconds
        Slf4jReporter reporter = Slf4jReporter.forRegistry(registry)
                .convertRatesTo(SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);
    }

}
