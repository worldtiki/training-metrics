package albuquerque.metrics.gauges;

import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;
import static com.codahale.metrics.MetricRegistry.name;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;

/**
 * A gauge is an instantaneous measurement of a value.
 *
 * In this example we create a list and periodically add items to it.
 * The console reporter should display the size of the list at the time it runs.
 */
public class GaugesExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(GaugesExample.class);
    private static final int CORE_POOL_SIZE = 1;
    private static final long INITIAL_DELAY = 0;
    private static final long INTERVAL_PERIOD = 1;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;

    private static MetricRegistry registry = new MetricRegistry();
    private static List<String> list = new ArrayList<String>();

    public static void main(String[] args) {
        // We start by naming our metric and registering it with the registry
        String metricName = name(GaugesExample.class, "list-size");
        registry.register(metricName, (Gauge<Integer>) () -> list.size());

        // We also schedule a task to periodically add items to our list
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        executor.scheduleAtFixedRate(() -> {
                    LOGGER.info("Adding new item to list");
                    list.add("dummy element");
                },
                INITIAL_DELAY, INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // Then we start our reporter and set it to run every couple of seconds
        Slf4jReporter reporter = Slf4jReporter.forRegistry(registry)
                .convertRatesTo(SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);

        // before stopping the executor service
        executor.shutdown();
    }
}
