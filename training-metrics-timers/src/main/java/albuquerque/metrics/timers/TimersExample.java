package albuquerque.metrics.timers;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;
import static com.codahale.metrics.MetricRegistry.name;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.Timer;

/**
 * A timer measures both the rate that a particular piece of code is called and the distribution of its duration.
 *
 * In this example we periodically call a method that takes a random amount of time to execute.
 * The console reporter should display both the number of calls and rate, as well as the min, max, mean (etc) values of the
 * execution times.
 */
public class TimersExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimersExample.class);
    private static final int CORE_POOL_SIZE = 1;
    private static final long INITIAL_DELAY = 0;
    private static final long INTERVAL_PERIOD = 1;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;
    private static Random random = new Random();

    private static MetricRegistry registry = new MetricRegistry();

    public static void main(String[] args) {
        // We start by naming our metric and registering it with the registry
        String metricName = name(TimersExample.class, "execution-time");
        Timer executionsTimer = registry.timer(metricName);

        // We also schedule a task to periodically call our dummy method
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        executor.scheduleAtFixedRate(
                () -> executeSomeOperation(executionsTimer),
                INITIAL_DELAY, INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // Then we start our reporter and set it to run every couple of seconds
        Slf4jReporter reporter = Slf4jReporter.forRegistry(registry)
                .convertRatesTo(SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);

        // before stopping the executor service
        executor.shutdown();
    }

    private static void executeSomeOperation(Timer executionsTimer) {
        LOGGER.info("Executing operation...");

        final Timer.Context context = executionsTimer.time();

        try {
            long startTime = System.currentTimeMillis();
            int randomExecutionTime = random.nextInt(500);

            sleepFor(randomExecutionTime, MILLISECONDS);

            long endTime = System.currentTimeMillis();
            long executionTime = endTime - startTime;

            LOGGER.info("Operation took: {} ms", executionTime);
        } finally {
            context.stop();
        }
    }
}
