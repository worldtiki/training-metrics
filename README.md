# Getting Started

This project may be used as a repository of examples of how to use the dropwizard metrics library.
It contains several maven submodules, each with a working example of a specific feature of the library.

## Dependencies

You need to setup the `metrics-core` library as a dependency.
You may need additional dependencies if you are using annotations, healthchecks, or if you want to integrate your application with Spring.
Please check the pom files on the examples for more information.

## Metrics types

This project covers most of the features of the metrics library.
You will find examples on the maven submodules. There is a bit of boilerplate common to all modules, namely setting up the metrics registry, where all metrics are registered, and setting up a reporter.
Note that to keep the code simple and consistent the same type of reporter is used for all examples. You can integrate your application with different reportes, for example JMX reporters, Graphite reporters, and others. 

| Module       | Description                                                                                                  |
| -------------|--------------------------------------------------------------------------------------------------------------|
| Counters     | Wrapper around a AtomicLong instance that can be incremented and decremented                                 |
| Gauges       | Instantaneous measurements of a value                                                                        |
| Timers       | Timers measure the rate a particular code is called and the distribution of it's duration                    |
| Histogram    | Histograms measure statistical distribution of values in a stream of data                                    |
| Healthchecks | Used to keep track of the health of your app and it's dependencies                                           |
| Annotations  | Example on how to integrate the metrics-library with spring and how to use annotations to simplify your code |
| JVM          | Built in metrics sets to analyze JVM statistics                                                              |


## References

- [Getting started - Dropwizard Metrics](https://dropwizard.github.io/metrics/3.1.0/getting-started/)
- [Api Docs - Dropwizard](https://dropwizard.github.io/metrics/3.1.0/apidocs/)
- [Spring integration for Dropwizard metrics](http://www.ryantenney.com/metrics-spring/)