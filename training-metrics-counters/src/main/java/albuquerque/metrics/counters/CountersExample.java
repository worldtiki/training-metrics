package albuquerque.metrics.counters;

import static java.util.concurrent.TimeUnit.SECONDS;

import static albuquerque.metrics.utils.Utils.sleepFor;
import static com.codahale.metrics.MetricRegistry.name;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;

/**
 * A counter is just a gauge for a Long instance. You can increment or decrement its value.
 *
 * In this example periodically simulate user logins.
 * The console reporter should display the current value for the counter.
 */
public class CountersExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountersExample.class);
    private static final int CORE_POOL_SIZE = 1;
    private static final long INITIAL_DELAY = 0;
    private static final long INTERVAL_PERIOD = 1;
    private static final TimeUnit INTERVAL_TIME_UNIT = SECONDS;

    private static MetricRegistry registry = new MetricRegistry();

    public static void main(String[] args) {
        // We start by naming our metric and registering it with the registry
        String metricName = name(CountersExample.class, "loggedInUsers");
        Counter loggedInUsersCounter = registry.counter(metricName);

        // We also schedule a task to periodically add items to our list
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
        executor.scheduleAtFixedRate(() -> {
                    userLogin(loggedInUsersCounter);
                },
                INITIAL_DELAY, INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // Then we start our reporter and set it to run every couple of seconds
        Slf4jReporter reporter = Slf4jReporter.forRegistry(registry)
                .convertRatesTo(SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(INTERVAL_PERIOD, INTERVAL_TIME_UNIT);

        // ...and we keep the app running for a bit
        sleepFor(10, SECONDS);

        // before stopping the executor service
        executor.shutdown();
    }

    private static void userLogin(Counter loggedInUsersCounter) {
        LOGGER.info("User login");

        // We increment the counter by one
        loggedInUsersCounter.inc();
    }
}
