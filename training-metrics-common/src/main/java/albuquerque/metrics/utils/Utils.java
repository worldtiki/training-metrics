package albuquerque.metrics.utils;

import java.util.concurrent.TimeUnit;

/**
 * Utility methods to improve readability on the examples.
 */
public class Utils {

    public static void sleepFor(int duration, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(duration);
        } catch (InterruptedException e) {
            // ignore
        }
    }
}
